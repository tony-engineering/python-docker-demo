# the-greatest-app-ever

This is a simple Python application to showcase good development practices. 

## Concept

As a user of “The greatest app ever” I would like and web app to request titles from
the page that I am looking for. The customer needs a REST server, users will make
GET request to it on /title/{page_address} and they will expect the title of the
provided page.  

The goal is to get this done in 2 hours 30 minutes with a clean architecture.

What I could achieve:
- Set up Flask + Docker
- HTML DOM parsing with test coverage (not handling edge cases)
- Documentation with architecture schema
- HTML page downloader
- Object Oriented design

! This app is not intended for production use. !

## Run

*Requirements: docker, docker-compose*  

`docker-compose up --build`  
Go to `http://0.0.0.0:5000/title/swapi.dev`. Result should be:  
```
{
    "page_title": "\n      SWAPI - The Star Wars API\n    "
}
```
Other example, go to `http://0.0.0.0:5000/title/duckduckgo.com`. Result should be:  
```
{
    "page_title": "DuckDuckGo — Privacy, simplified."
}
```


## Test API

*Requirements: docker, bash*  

`cd api`  
`./run-tests.sh`  

Result should be (2 tests are still WIP, see TODOs section):
```
Ran 7 tests in 0.002s

FAILED (failures=2)
```

## Architecture

```
                     +---------------+
                     | Python REST   |
                     | API           |
                     |               |
                     | built with    |
                     | Flask         |
                     |               |
                     |               |
                     |               |
                     +-----+----+----+
                           ^    |
Request:                   |    | Response:
/title/swapi.dev           |    | {"page_title": "SWAPI -
                           |    | The Star Wars API"}
                           |    v
                     +-----+----+----+
                     | HTTP Client   |
                     |               |
                     | web browser,  |
                     | curl, ..      |
                     |               |
                     |               |
                     |               |
                     |               |
                     +---------------+
```
[edit schema](https://asciiflow.com)

Note: do not put "https://" or "http://" in the url path. "https://" is automatically added as prefix. Thus, the
domain name given as argument must handle "https" protocol.

## TODOs

If you want to contribute to this great app, here are some ideas:
- Improve the PageTitleExtractor to handle edge cases. Make test_instanciate_success_3 and test_instanciate_success_4 pass.
- Create the client module (in the `client` folder), and serve it with an Nginx server. 
Add the client module to the docker-compose setup so it can communicate with the API.