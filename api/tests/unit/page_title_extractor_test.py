import unittest
from page_title_extractor import PageTitleExtractor


class PageTitleExtractorTestCase(unittest.TestCase):

    def setUp(cls):
        pass

    def test_instanciate_class(self):
        htmlDom = "<html><head><title>SWAPI - The Star Wars API</title></head></html>"
        PageTitleExtractor(htmlDom)

    def test_instanciate_success_1(self):
        htmlDom = "<html><head><title>SWAPI - The Star Wars API</title></head></html>"
        pageTitleExtractor = PageTitleExtractor(htmlDom)
        assert pageTitleExtractor.extract().title == "SWAPI - The Star Wars API"

    def test_instanciate_success_2(self):
        htmlDom = "<html><head><title> </title></head></html>"
        pageTitleExtractor = PageTitleExtractor(htmlDom)
        assert pageTitleExtractor.extract().title == " "

    # def test_instanciate_success_3(self):
    #     htmlDom = "<html><head><title></title></head></html>"
    #     pageTitleExtractor = PageTitleExtractor(htmlDom)
    #     assert pageTitleExtractor.extract().title == ""

    # def test_instanciate_success_4(self):
    #     htmlDom = "<html><head><title><some_tricky_title></title></head></html>"
    #     pageTitleExtractor = PageTitleExtractor(htmlDom)
    #     assert pageTitleExtractor.extract().title == "<some_tricky_title>"

    def test_instanciate_success_5(self):
        htmlDom = "<title>SWAPI - The Star Wars API</title>"
        pageTitleExtractor = PageTitleExtractor(htmlDom)
        assert pageTitleExtractor.extract().title == "SWAPI - The Star Wars API"

    def test_instanciate_class_wrong_input_1(self):
        htmlDom = "SWAPI - The Star Wars API"
        pageTitleExtractor = PageTitleExtractor(htmlDom)
        assert pageTitleExtractor.extract().title == None
