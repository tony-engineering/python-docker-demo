from custom_html_parser import CustomHtmlParser


class PageTitleExtractor:
    def __init__(self, htmlDom: str):
        self.htmlDom = htmlDom
        self.title = None

    def extract(self):
        parser = CustomHtmlParser()
        parser.feed(self.htmlDom)
        self.title = parser.title
        return self
