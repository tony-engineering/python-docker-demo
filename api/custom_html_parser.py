from html.parser import HTMLParser


class CustomHtmlParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self.recording = 0
        self.title = None

    def handle_starttag(self, tag, attrs):
        print("")
        if tag == 'title':
            self.recording = 1

    def handle_endtag(self, tag):
        if tag == 'title':
            self.recording -= 1

    def handle_data(self, data):
        if self.recording:
            print("Found data:"+data)
            self.title = data
